package pl.edu.agh.sm.smlab3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {

    EditText ipInput;
    EditText loginInput;

    public static final String IP = "IP";
    public static final String LOGIN = "NICK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ipInput = (EditText)findViewById(R.id.ipInput);
        loginInput = (EditText)findViewById(R.id.loginInput);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonClicked(View v){
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        intent.putExtra(IP, ipInput.getText().toString());
        intent.putExtra(LOGIN, loginInput.getText().toString());
        startActivity(intent);

    }
}
