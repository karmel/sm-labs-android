//
//  ViewController.m
//  MQTTTry
//
//  Created by Matrejek, Mateusz on 08/04/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import "ViewController.h"
#import <MQTTKit/MQTTKit.h>

@interface ViewController ()

@property MQTTClient *client;
@property NSString *clientID;
@property NSString *host;
@property NSMutableArray *messages;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.listView setDataSource:self];
    [self.navigationItem setTitle:@"MQTT CHAT"];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blueColor]];

    [self.navigationController.navigationBar setBackgroundColor:[UIColor blueColor]];
    [self.navigationController.navigationBar
        setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    self.messages = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onConnect:(id)sender {

    self.clientID = self.loginInput.text;
    NSString *ip = self.ipInput.text;
    self.host = [@"" stringByAppendingString:ip];

    NSLog(@"Host = %@, ClientID = %@ ", self.host, self.clientID);
    self.client = [[MQTTClient alloc] initWithClientId:self.clientID];
    [self.client setCleanSession:TRUE];
    [self.client connectToHost:self.host
             completionHandler:^(NSUInteger code) {

               if (code) {
                   NSLog(@"Connection Error %lu", (unsigned long)code);
               }

             }];

    __weak ViewController *weakSelf = self;

    [self.client setMessageHandler:^(MQTTMessage *message) {
      NSString *text = message.payloadString;
      NSString *topic = message.topic;
      [[weakSelf messages] addObject:message];
      [[weakSelf listView] reloadData];
      NSLog(@"Received message %@ %@", text, topic);
    }];

    [self.client subscribe:@"#" withCompletionHandler:nil];
}
- (IBAction)onSend:(id)sender {

    NSString *msg = self.msgInput.text;

    [self.client publishString:msg
                       toTopic:self.clientID
                       withQos:AtMostOnce
                        retain:NO
             completionHandler:^(int mid) {
               NSLog(@"message has been delivered");
             }];
}

#pragma mark UITableViewDatasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    MQTTMessage *msg = (MQTTMessage *)[self.messages objectAtIndex:indexPath.row];

    cell.textLabel.text = msg.payloadString;
    cell.detailTextLabel.text = msg.topic;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messages.count;
}

@end
