package pl.edu.agh.sm.rpncalculator;

import java.util.LinkedList;

/**
 * Created by Mateusz Matrejek on 25/03/15.
 */
public class RPNLogic {

    public static final int RPN_ID_X=0;
    public static final int RPN_ID_Y=1;
    public static final int RPN_ID_Z=2;
    public static final int RPN_ID_T=3;

    //stos powinien zawierac przynajmniej jeden element o indeksie 0!
    LinkedList<Double> stack;

    public RPNLogic() {
        super();
        stack = new LinkedList<Double>();
        stack.addFirst(0.0);
    }

    public void input(Double value){
        stack.removeFirst();
        stack.addFirst(value);
    }

    public void enter() {
        Double temp = stack.getFirst();
        stack.addFirst(temp);
    }

    public void div() {
        if (stack.size()>=2){
            Double temp1 = stack.getFirst();
            stack.removeFirst();
            Double temp2 = stack.getFirst();
            stack.removeFirst();
            stack.addFirst(temp2/temp1);
        }
    }

    public void add() {
        if (stack.size()>=2){
            Double temp1 = stack.getFirst();
            stack.removeFirst();
            Double temp2 = stack.getFirst();
            stack.removeFirst();
            stack.addFirst(temp1+temp2);
        }
    }

    public void sub() {
        if (stack.size()>=2){
            Double temp1 = stack.getFirst();
            stack.removeFirst();
            Double temp2 = stack.getFirst();
            stack.removeFirst();
            stack.addFirst(temp2-temp1);
        }
    }

    public void mul() {
        if (stack.size()>=2){
            Double temp1 = stack.getFirst();
            stack.removeFirst();
            Double temp2 = stack.getFirst();
            stack.removeFirst();
            stack.addFirst(temp1*temp2);
        }
    }

    public int size(){
        return stack.size();
    }

    public Double getElement(int index){
        if (index<stack.size()){
            return stack.get(index);
        } else return null;
    }

    public void clear() {
        stack.clear();
        stack.addFirst(0.0);
    }

}
