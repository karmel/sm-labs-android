package pl.edu.agh.sm.rpncalculator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * Created by Mateusz Matrejek on 24/03/15.
 */
public class SimpleRPNService extends Service {


    private final ISimpleRPNService.Stub mBinder = new ISimpleRPNService.Stub() {
        @Override
        public double getStackX() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_X);
        }

        @Override
        public double getStackY() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_Y);
        }

        @Override
        public double getStackZ() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_Z);
        }

        @Override
        public double getStackT() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_T);
        }

        @Override
        public boolean isStackX() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_X) != null;
        }

        @Override
        public boolean isStackY() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_Y) != null;
        }

        @Override
        public boolean isStackZ() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_Z) != null;
        }

        @Override
        public boolean isStackT() throws RemoteException {
            return rpnLogicUnit.getElement(RPNLogic.RPN_ID_T) != null;
        }

        @Override
        public void modifyStackX(double value) throws RemoteException {
            rpnLogicUnit.input(value);
        }

        @Override
        public void clear() throws RemoteException {
            rpnLogicUnit.clear();
        }

        @Override
        public void pressEnter() throws RemoteException {
            rpnLogicUnit.enter();
        }

        @Override
        public void pressAdd() throws RemoteException {
            rpnLogicUnit.add();
        }

        @Override
        public void pressSub() throws RemoteException {
            rpnLogicUnit.sub();
        }

        @Override
        public void pressDiv() throws RemoteException {
            rpnLogicUnit.div();
        }

        @Override
        public void pressMul() throws RemoteException {
            rpnLogicUnit.mul();
        }
    };


    private RPNLogic rpnLogicUnit;

    @Override
    public void onCreate() {
        super.onCreate();
        rpnLogicUnit = new RPNLogic();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return mBinder;
    }
}